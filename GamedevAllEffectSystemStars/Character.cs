﻿using System;
using System.Collections.Generic;

namespace GamedevAllEffectSystemStars
{
    public class Character
    {
        public event Action<Character> EventDead;

        public string Name { get; set; }
        public Armor Armor { get; private set; }
        public Weapon Weapon { get; private set; }

        public List<Effect> Effects { get; private set; } = new List<Effect>();

        public float Health
        {
            get { return m_health; }
            set
            {
                m_health = Math.Max(value, 0.0f);
                if (m_health <= 0.0f)
                {
                    EventDead?.Invoke(this);
                }
            }
        }

        private float m_health = float.MinValue;

        public Character(string name, Armor armor, Weapon weapon, float initialHealth)
        {
            Name = name;
            Armor = armor;
            Weapon = weapon;
            Health = initialHealth;

            AddEffects(armor.Effects);
        }

        public void Tick()
        {
            // Чтобы не инвалидировать итератор в foreach, т.к. если заюзать for (int i...), то где-то будет страдать один Ариксу
            // И не надо мне втирать про GC и феншуй, я бы сделал через for
            var effectsToRemove = new LinkedList<Effect>();

            foreach (var effect in Effects)
            {
                effect.Tick();

                if (effect.IsFinished)
                {
                    effectsToRemove.AddLast(effect);
                }
            }

            foreach (var effect in effectsToRemove)
            {
                effect.ApplyBeforeRemovedFromCharacter();
                Effects.Remove(effect);
            }
        }

        public void ReceiveHit(Weapon weapon)
        {
            Console.WriteLine(Name + " receives a hit");

            AddEffects(weapon.Effects);
        }

        public void AddEffects(List<Effect> effects)
        {
            foreach (var effectPrototype in effects)
            {
                var effect = effectPrototype.GetShallowCopy();

                Effects.Add(effect);
                effect.TargetCharacter = this;
                effect.ApplyOnAdded();
            }
        }

        public string GetStatus()
        {
            return Name + ", hp: " + Health + ", Effects: " + string.Join(", ", Effects);
        }
    }
}
