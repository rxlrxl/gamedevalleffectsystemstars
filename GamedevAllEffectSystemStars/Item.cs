﻿using System.Collections.Generic;

namespace GamedevAllEffectSystemStars
{
    public class Item
    {
        public List<Effect> Effects { get; set; } = new List<Effect>();
    }
}
