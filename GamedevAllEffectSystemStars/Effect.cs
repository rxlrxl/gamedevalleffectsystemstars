﻿namespace GamedevAllEffectSystemStars
{
    public abstract class Effect
    {
        public Character TargetCharacter { get; set; }

        public bool IsFinished { get; protected set; }

        public virtual void ApplyOnAdded() { }
        public virtual void ApplyBeforeRemovedFromCharacter() { }
        public virtual void Tick() { }

        public override string ToString()
        {
            return GetType().Name.ToString();
        }

        public Effect GetShallowCopy()
        {
            return (Effect)MemberwiseClone();
        }
    }
}
