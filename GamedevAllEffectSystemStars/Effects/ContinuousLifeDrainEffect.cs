﻿namespace GamedevAllEffectSystemStars.Effects
{
    public class ContinuousLifeDrainEffect : TimedEffect
    {
        public readonly float DamagePerTick;

        public ContinuousLifeDrainEffect(float damagePerTick, int lifetimeTicks) : base(lifetimeTicks)
        {
            DamagePerTick = damagePerTick;
        }

        public override void Tick()
        {
            base.Tick();

            TargetCharacter.Health -= DamagePerTick;

            // Тут начинается псевдокод логики
            //    public class SyncThreadedTCPSocketTransportProvider : ITransportProvider
            //{
            //    public Action<byte[]> EventDataReceived { get; set; }
            //    public Action<DisconnectReason> EventDisconnected { get; set; }

            //    public bool IsConnected { get { return _isConnected; } }
            //    public IPAddress Address { get; private set; }
            //    public ushort Port { get; private set; }

            //    private readonly Socket _socket;
            //    private readonly Queue<byte[]> _outboundDataQueue = new Queue<byte[]>(); // Use ConcurrentQueue?

            //    private readonly int _loopWaitingTimeMilliseconds;
            //    private volatile bool _isConnected = false;

            //    public SyncThreadedTCPSocketTransportProvider(int loopWaitingTimeMilliseconds)
            //    {
            //        _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //        _loopWaitingTimeMilliseconds = loopWaitingTimeMilliseconds;
            //    }

            //            var thread = new Thread(ExecuteNetworkingLoop);
            //            thread.Name = "SyncThreadedTCPSocket_" + Address + ":" + Port;
            //            thread.Start();
            //        }
            //    }

            //    public void Connect(IPAddress address, ushort port, Action<ITransportProvider> onConnected = null, Action<string> onFailed = null)
            //    {
            //        var thread = new Thread(() =>
            //        {

            //            ExecuteNetworkingLoop();
            //        });
            //        thread.Name = "SyncThreadedTCPSocket_" + address + ":" + port;
            //        thread.Start();
            //    }

            //    public void Disconnect()
            //    {
            //    }

            //        reason = DisconnectReason.Undefined;
            //        return false;
            //    }
            //}
            // А тут он заканчивается
        }
    }
}
