﻿namespace GamedevAllEffectSystemStars.Effects
{
    public class TimedEffect : Effect
    {
        public readonly int LifetimeTicks;

        public int TicksPassed { get; private set; }

        public TimedEffect(int ticks)
        {
            LifetimeTicks = ticks;
        }

        public override void Tick()
        {
            base.Tick();

            ++TicksPassed;
            if (TicksPassed >= LifetimeTicks)
            {
                IsFinished = true;
            }
        }
    }
}
