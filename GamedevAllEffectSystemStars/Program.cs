﻿using GamedevAllEffectSystemStars.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace GamedevAllEffectSystemStars
{
    public class Program
    {
        private static Random s_random = new Random();

        public static void Main(string[] args)
        {
            var capitalist = new Character(
                "Capitalist", 
                new Armor(), 
                new Weapon() { Effects = GetRandomEffects() },
                100.0f);
            var communist = new Character(
                "Communist",
                new Armor(),
                new Weapon() { Effects = GetRandomEffects() },
                100.0f);

            Console.WriteLine("Characters generated");
            Console.WriteLine("Capitalist's weapon effects: " + string.Join(", ", capitalist.Weapon.Effects));
            Console.WriteLine("Communist's weapon effects: " + string.Join(", ", capitalist.Weapon.Effects));
            Console.WriteLine("Let's get this party started");

            var game = new Game();
            game.Start(capitalist, communist);

            int ticksPassed = 0;
            while (!game.IsFinished) // Main loop и все такое
            {
                game.Tick();

                Console.WriteLine("Tick passed. Let's see...");

                if (ticksPassed % 10 == 2)
                {
                    int firstHitReceiverIndex = s_random.Next() % 2; // Потому что все должно быть честно
                    var firstHitReceiver = game.Characters[firstHitReceiverIndex];
                    var secondHitReceiver = game.Characters[firstHitReceiverIndex == 0 ? 1 : 0];

                    firstHitReceiver.ReceiveHit(secondHitReceiver.Weapon);
                    secondHitReceiver.ReceiveHit(firstHitReceiver.Weapon);
                }

                Console.WriteLine(game.GetStatus());

                Thread.Sleep(500);
                ++ticksPassed;
            }
            game.Finish();

            Console.WriteLine("\nPress enter to exit");
            Console.ReadLine();
        }

        private static List<Effect> GetRandomEffects()
        {
            int effectsCount = s_random.Next() % 3 + 1;

            var effects = new List<Effect>(effectsCount);
            for (int i = 0; i < effectsCount; ++i)
            {
                effects.Add(GetRandomEffect());
            }

            return effects;
        }

        private static Effect GetRandomEffect()
        {
            // Тут могла быть ваша фабрика

            int effectType = s_random.Next() % 4;
            switch (effectType)
            {
                case 0:
                    return new RemoveHealthOnAddEffect(s_random.Next() % 5 + 1);
                case 1:
                    return new ContinuousLifeDrainEffect(s_random.Next() % 3 + 1, s_random.Next() % 5 + 1);
                case 2:
                    return new CurseEffect(s_random.Next() % 5 + 1, s_random.Next() % 3 + 1, s_random.Next() % 5 + 1);
                case 3:
                    return new DelayedDamageEffect(s_random.Next() % 5 + 1, s_random.Next() % 5 + 1);
                default:
                    throw new Exception("Effect with type " + effectType + " is not defined");
            }
        }
    }

    public class Game
    {
        public bool IsFinished { get; private set; }

        public List<Character> Characters { get; private set; }

        public void Start(params Character[] characters)
        {
            Characters = characters.ToList();

            foreach (var character in Characters)
            {
                character.EventDead += OnCharacterDead;
            }
        }

        public void Finish()
        {
            foreach (var character in Characters)
            {
                character.EventDead -= OnCharacterDead;
            }
        }

        public void Tick()
        {
            foreach (var character in Characters)
            {
                character.Tick();
            }
        }

        public string GetStatus()
        {
            var builder = new StringBuilder();
                
            foreach (var character in Characters)
            {
                builder.AppendLine(character.GetStatus());
            }

            return builder.ToString();
        }

        private void OnCharacterDead(Character character)
        {
            Console.WriteLine(character.Name + " is dead.");

            IsFinished = true;
        }
    }
}
